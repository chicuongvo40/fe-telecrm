import 'react-datepicker/dist/react-datepicker.css';
import { useState } from 'react';
import { useEffect } from 'react';
import { useQuery } from '@tanstack/react-query';
import { useAxios } from '~/context/AxiosContex';
import { AddCampaignAdm } from '~/components/Addition/AcceptedCustomer';
import styles from './CallReport.module.css';
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
export default function AcceptCustomer() {
  const [id, setId] = useState();
  const [xstaff, setStaff] = useState();
  const { getcustomerrequest } = useAxios();
  const [open, setOpen] = useState(false);

  const handleOpen = (id) => {
    setId(id);
    setOpen(true);
  }
  const handleClose = () => {console.log('có chạy handle close');setOpen(false)};
  const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 4,
  };
  const {
    data: staff,
    isLoading,
    refetch
  } = useQuery(
    {
      queryKey: ['staff'],
      queryFn: () => getcustomerrequest(),
    }
  );
  useEffect(() => {
    if (staff) {
      setStaff(staff);
      console.log(staff);
    }
  }, [staff]);


  return (
    <>
      {open ? (
        <AddCampaignAdm
          refetch={refetch}
          setOpenModal={setOpen}
          idBrand={id}
        />
      ) : null}


      <div className={styles.listCall}>
        <div className={styles.box__content}>
          <div className={styles.head__box}>
            <h3>Yêu Cầu từ Khác Hàng</h3>
          </div>
          <div className={styles.head_new}>

            <span>STT</span>
            <span>SĐT</span>
            <span>Tên</span>
            <span>TIêu Đề</span>
            <span>Accept</span>

          </div>

          {staff != null && xstaff != null &&
            xstaff.map((htr, index) => (
              <div
                className={styles.content_new}
              >
                <span>{index + 1}</span>
                <span>{htr.customerId ? htr.customerId.phoneNumber : 'N/A'}</span>
                <span>{htr.customerId ? htr.customerId.name : 'N/A'}</span>
                <span >
                  {htr.title}
                </span>

                <button onClick={() => handleOpen(htr.id)}>
                  <AddCircleOutlineIcon />
                </button>
              </div>
            ))}
        </div>
      </div>
    </>
  );
}
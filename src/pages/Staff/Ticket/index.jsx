import React from 'react';
import {
  Box,
  Card,
  CardContent,
  Button,
  Typography,
  Grid,
  Menu, MenuItem
} from "@mui/material";
import { useNavigate } from 'react-router-dom';
import { useQuery } from '@tanstack/react-query';
import { useState, useEffect } from 'react';
//Context
import { useAxios } from '~/context/AxiosContex';
//Utils
import { formartDate } from '~/utils/functions';
//Components
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';
import PhoneIcon from '@mui/icons-material/Phone';
import KeyIcon from '@mui/icons-material/Key';
import FilterListIcon from '@mui/icons-material/FilterList';
import SearchIcon from '@mui/icons-material/Search';
import DebouncedInput from "~/components/Material/DebouncedInput/DebouncedInput";
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import { useAuth } from '~/context/AuthContext';
import { format } from 'date-fns';
export default function Ticket() {
  const { user } = useAuth();
  //Axios
  const { getTicketByStatus } = useAxios();
  //Usestate
  const [ticketData, setTicketData] = useState([]);
  const [ticketData1, setTicketData1] = useState([]);
  const [ticketData2, setTicketData2] = useState([]);
  const [ticketData3, setTicketData3] = useState([]);

  const [date, setDate] = useState();
  const [anchorEl, setAnchorEl] = useState(null);
  const [selectedDate, setSelectedDate] = useState(null);
  const [search, setSearch] = useState(null);
  const handleChange = date => {
    setSelectedDate(date);
  };

  const handleMenuOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
  };
  //Navigation
  const navigate = useNavigate();
  //GetTicket
  const {
    data: ticket,
  } = useQuery(
    {
      queryKey: ['ticket'],
      queryFn: () => getTicketByStatus(Object.values(user)[4]),
    }
  );

  useEffect(() => {
    if (ticket) {
      console.log(ticket.statusOne);
      setTicketData(ticket.statusOne);
      setTicketData1(ticket.statusTwo);
      setTicketData2(ticket.statusThree);
      setTicketData3(ticket.statusFour);
    }

  }, [ticket]);


  return (
    <>
      {
        ticket &&
        <Box width='80vw'>
          <Box display='block'>
            <div className="w-full flex items-center gap-1">
              <SearchIcon />
              <DebouncedInput
                value={search ?? ""}
                onChange={(value) => setSearch(value)}
                className="p-2 bg-transparent outline-none  w-1/5  border-blue-gray-900"
                placeholder="Search by phonenumber"
              />
              <CalendarMonthIcon sx={{ marginLeft: 5 }} />
              <DatePicker
                selected={selectedDate}
                onChange={handleChange}
                dateFormat="yyyy-MM-dd"
                placeholderText="Select a date"
              />
              {selectedDate && <Button onClick={() => setSelectedDate(null)}>Clear</Button>}
            </div>
          </Box>

          <Box mt="1.5rem" gridTemplateColumns="repeat(4,1fr)" height='600px' display='block'>
            <Grid container spacing={2}>

              <Grid item xs={3}>
                <Box sx={{
                  bgcolor: '#F6F5F2',
                  ml: 2,
                  borderRadius: '6px',
                  height: 'fit-content',
                  maxHeight: '600px'
                }}>
                  <Box sx={{
                    height: '50px',
                    p: 2,
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'space-between'
                  }}>
                    <Typography sx={{
                      fontWeight: 'bold',
                      cursor: 'pointer',
                      color: 'green'
                    }}>Tiếp Nhận ({ticketData.length})</Typography>
                    <FilterListIcon sx={{}} />
                  </Box>
                  <Box sx={{
                    '&::-webkit-scrollbar:hover': {
                      display: 'display !important' // Override display property on hover
                    },
                    p: 2,
                    display: 'flex',
                    flexDirection: 'column',
                    gap: 1,
                    overflowX: 'hidden',
                    overflowY: 'auto', // Change 'overflow' to 'overflowY' for vertical scrolling
                    maxHeight: '520px'
                  }}>
                    <div>
                      {
                        ticketData && ticketData.length > 0 && ticketData.filter((val) => {
                          if (selectedDate === null && search === null) {
                            return true;
                          }
                          if (selectedDate !== null && search !== null) {
                        
                            console.log(format(selectedDate, 'dd/MM/yyyy'));
                            return format(val.createdDate, 'dd/MM/yyyy') === format(selectedDate, 'dd/MM/yyyy') && val.customerId.phoneNumber.includes(search);
                          }
                          if (selectedDate !== null && search === null) {
                            return format(val.createdDate, 'dd/MM/yyyy') === selectedDate.toLocaleDateString();
                          }
                          if (selectedDate === null && search !== null) {
                            return val.customerId.phoneNumber.includes(search);
                          }
                          return false;
                        })
                          .map(item => (
                            <Card
                              key={item.id}
                              sx={{
                                cursor: 'pointer',
                                boxShadow: '0 1px 1px rgba(0,0,0,0.2)',
                                overflow: 'unset',
                                marginBottom: '10px'
                              }}
                            >
                              <CardContent
                                sx={{
                                  p: 1.5,
                                  '&:last-child': { p: 1.5 }
                                }}
                              >
                                <p className='flex justify-between'>
                                  <Typography sx={{ fontSize: '12px', fontWeight: 'bold' }}>
                                    {item.title}
                                  </Typography>
                                  <Button  onClick={() => navigate(`/${item.id}/${item.customerId.id}/ticket`)} >
                                  <Typography sx={{ fontSize: '12px', fontWeight: 'bold', color: 'blue' }}>
                                    Detail
                                  </Typography>
                                  </Button>
                                </p>
                                <Typography sx={{ fontSize: '10px', fontWeight: 'bold' }}>
                                  <KeyIcon fontSize="small" /> TK32132{item.id}
                                </Typography>
                                <div className="flex justify-between">
                                  <Typography sx={{ fontSize: '10px', fontWeight: 'bold' }}>
                                    <PhoneIcon fontSize="small" /> {item.customerId.phoneNumber}
                                  </Typography>
                                  <Typography sx={{ fontSize: '10px', fontWeight: 'bold' }}>
                                    <CalendarMonthIcon fontSize="small" /> {formartDate(item.createdDate, 'short')}
                                  </Typography>
                                </div>
                              </CardContent>
                            </Card>
                          ))}
                    </div>

                  </Box>

                </Box>
              </Grid>

              <Grid item xs={3}>
                <Box sx={{
                  bgcolor: '#F6F5F2',
                  ml: 2,
                  borderRadius: '6px',
                  height: 'fit-content',
                  maxHeight: '600px'
                }}>
                  <Box sx={{
                    height: '50px',
                    p: 2,
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'space-between'
                  }}>
                    <Typography sx={{
                      fontWeight: 'bold',
                      cursor: 'pointer',
                      color: '#F9C46B'
                    }}>Đang Xử Lý ({ticketData1.length})</Typography>
                    <FilterListIcon sx={{}} />
                  </Box>
                  <Box sx={{
                    '&::-webkit-scrollbar:hover': {
                      display: 'display !important' // Override display property on hover
                    },
                    p: 2,
                    display: 'flex',
                    flexDirection: 'column',
                    gap: 1,
                    overflowX: 'hidden',
                    overflowY: 'auto', // Change 'overflow' to 'overflowY' for vertical scrolling
                    maxHeight: '520px'
                  }}>
                    <div>
                      {
                        ticketData1 && ticketData1.length > 0 && ticketData1.filter((val) => {
                          if (selectedDate === null && search === null) {
                            return true;
                          }
                          if (selectedDate !== null && search !== null) {
                          
                            console.log(format(selectedDate, 'dd/MM/yyyy'));
                            return format(val.createdDate, 'dd/MM/yyyy') === format(selectedDate, 'dd/MM/yyyy') && val.customerId.phoneNumber.includes(search);
                          }
                          if (selectedDate !== null && search === null) {
                            return format(val.createdDate, 'dd/MM/yyyy') === selectedDate.toLocaleDateString();
                          }
                          if (selectedDate === null && search !== null) {
                            return val.customerId.phoneNumber.includes(search);
                          }
                          return false;
                        })
                          .map(item => (
                            <Card
                              key={item.id}
                              sx={{
                                cursor: 'pointer',
                                boxShadow: '0 1px 1px rgba(0,0,0,0.2)',
                                overflow: 'unset',
                                marginBottom: '10px'
                              }}
                            >
                              <CardContent
                                sx={{
                                  p: 1.5,
                                  '&:last-child': { p: 1.5 }
                                }}
                              >
                                <p className='flex justify-between'>
                                  <Typography sx={{ fontSize: '12px', fontWeight: 'bold' }}>
                                  {item.title}
                                  </Typography>
                                  <Button  onClick={() => navigate(`/${item.id}/${item.customerId.id}/ticket`)} >
                                  <Typography sx={{ fontSize: '12px', fontWeight: 'bold', color: 'blue' }}>
                                    Detail
                                  </Typography>
                                  </Button>
                                </p>
                                <Typography sx={{ fontSize: '10px', fontWeight: 'bold' }}>
                                  <KeyIcon fontSize="small" /> TK32132{item.id}
                                </Typography>
                                <div className="flex justify-between">
                                  <Typography sx={{ fontSize: '10px', fontWeight: 'bold' }}>
                                    <PhoneIcon fontSize="small" /> {item.customerId.phoneNumber}
                                  </Typography>
                                  <Typography sx={{ fontSize: '10px', fontWeight: 'bold' }}>
                                    <CalendarMonthIcon fontSize="small" /> {formartDate(item.createdDate, 'short')}
                                  </Typography>
                                </div>
                              </CardContent>
                            </Card>
                          ))}
                    </div>

                  </Box>

                </Box>
              </Grid>

              <Grid item xs={3}>
                <Box sx={{
                  bgcolor: '#F6F5F2',
                  ml: 2,
                  borderRadius: '6px',
                  height: 'fit-content',
                  maxHeight: '600px'
                }}>
                  <Box sx={{
                    height: '50px',
                    p: 2,
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'space-between'
                  }}>
                    <Typography sx={{
                      fontWeight: 'bold',
                      cursor: 'pointer',
                      color: '#2A6FDB'
                    }}>Đã Xử Lý ({ticketData2.length})</Typography>
                    <FilterListIcon sx={{}} />
                  </Box>
                  <Box sx={{
                    '&::-webkit-scrollbar:hover': {
                      display: 'display !important' // Override display property on hover
                    },
                    p: 2,
                    display: 'flex',
                    flexDirection: 'column',
                    gap: 1,
                    overflowX: 'hidden',
                    overflowY: 'auto', // Change 'overflow' to 'overflowY' for vertical scrolling
                    maxHeight: '520px'
                  }}>
                    <div>
                      {
                        ticketData2 && ticketData2.length > 0 && ticketData2.filter((val) => {
                          if (selectedDate === null && search === null) {
                            return true;
                          }
                          if (selectedDate !== null && search !== null) {
                        
                            console.log(format(selectedDate, 'dd/MM/yyyy'));
                            return format(val.createdDate, 'dd/MM/yyyy') === format(selectedDate, 'dd/MM/yyyy') && val.customerId.phoneNumber.includes(search);
                          }
                          if (selectedDate !== null && search === null) {
                            return format(val.createdDate, 'dd/MM/yyyy') === selectedDate.toLocaleDateString();
                          }
                          if (selectedDate === null && search !== null) {
                            return val.customerId.phoneNumber.includes(search);
                          }
                          return false;
                        })
                          .map(item => (
                            <Card
                              key={item.id}
                              sx={{
                                cursor: 'pointer',
                                boxShadow: '0 1px 1px rgba(0,0,0,0.2)',
                                overflow: 'unset',
                                marginBottom: '10px'
                              }}
                            >
                              <CardContent
                                sx={{
                                  p: 1.5,
                                  '&:last-child': { p: 1.5 }
                                }}
                              >
                                <p className='flex justify-between'>
                                  <Typography sx={{ fontSize: '12px', fontWeight: 'bold' }}>
                                  {item.title}
                                  </Typography>
                                  <Button  onClick={() => navigate(`/${item.id}/${item.customerId.id}/ticket`)} >
                                  <Typography sx={{ fontSize: '12px', fontWeight: 'bold', color: 'blue' }}>
                                    Detail
                                  </Typography>
                                  </Button>
                                </p>
                                <Typography sx={{ fontSize: '10px', fontWeight: 'bold' }}>
                                  <KeyIcon fontSize="small" /> TK32132{item.id}
                                </Typography>
                                <div className="flex justify-between">
                                  <Typography sx={{ fontSize: '10px', fontWeight: 'bold' }}>
                                    <PhoneIcon fontSize="small" /> {item.customerId.phoneNumber}
                                  </Typography>
                                  <Typography sx={{ fontSize: '10px', fontWeight: 'bold' }}>
                                    <CalendarMonthIcon fontSize="small" /> {formartDate(item.createdDate, 'short')}
                                  </Typography>
                                </div>
                              </CardContent>
                            </Card>
                          ))}
                    </div>

                  </Box>

                </Box>
              </Grid>

              <Grid item xs={3}>
                <Box sx={{
                  bgcolor: '#F6F5F2',
                  ml: 2,
                  borderRadius: '6px',
                  height: 'fit-content',
                  maxHeight: '600px'
                }}>
                  <Box sx={{
                    height: '50px',
                    p: 2,
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'space-between'
                  }}>
                    <Typography sx={{
                      fontWeight: 'bold',
                      cursor: 'pointer',
                      color: '#503A65'
                    }}>Kết Thúc ({ticketData3.length})</Typography>
                    <FilterListIcon sx={{}} />
                  </Box>
                  <Box sx={{
                    '&::-webkit-scrollbar:hover': {
                      display: 'display !important' // Override display property on hover
                    },
                    p: 2,
                    display: 'flex',
                    flexDirection: 'column',
                    gap: 1,
                    overflowX: 'hidden',
                    overflowY: 'auto', // Change 'overflow' to 'overflowY' for vertical scrolling
                    maxHeight: '520px'
                  }}>
                    <div>
                      {
                        ticketData3 && ticketData3.length > 0 && ticketData3.filter((val) => {
                          if (selectedDate === null && search === null) {
                            return true;
                          }
                          if (selectedDate !== null && search !== null) {
                         
                            console.log(format(selectedDate, 'dd/MM/yyyy'));
                            return format(val.createdDate, 'dd/MM/yyyy') === format(selectedDate, 'dd/MM/yyyy') && val.customerId.phoneNumber.includes(search);
                          }
                          if (selectedDate !== null && search === null) {
                            return format(val.createdDate, 'dd/MM/yyyy') === selectedDate.toLocaleDateString();
                          }
                          if (selectedDate === null && search !== null) {
                            return val.customerId.phoneNumber.includes(search);
                          }
                          return false;
                        })
                          .map(item => (
                            <Card
                              key={item.id}
                              sx={{
                                cursor: 'pointer',
                                boxShadow: '0 1px 1px rgba(0,0,0,0.2)',
                                overflow: 'unset',
                                marginBottom: '10px'
                              }}
                            >
                              <CardContent
                                sx={{
                                  p: 1.5,
                                  '&:last-child': { p: 1.5 }
                                }}
                              >
                                <p className='flex justify-between'>
                                  <Typography sx={{ fontSize: '12px', fontWeight: 'bold' }}>
                                  {item.title}
                                  </Typography>
                                  <Button  onClick={() => navigate(`/${item.id}/${item.customerId.id}/ticket`)} >
                                  <Typography sx={{ fontSize: '12px', fontWeight: 'bold', color: 'blue' }}>
                                    Detail
                                  </Typography>
                                  </Button>
                                </p>
                                <Typography sx={{ fontSize: '10px', fontWeight: 'bold' }}>
                                  <KeyIcon fontSize="small" /> TK32132{item.id}
                                </Typography>
                                <div className="flex justify-between">
                                  <Typography sx={{ fontSize: '10px', fontWeight: 'bold' }}>
                                    <PhoneIcon fontSize="small" /> {item.customerId.phoneNumber}
                                  </Typography>
                                  <Typography sx={{ fontSize: '10px', fontWeight: 'bold' }}>
                                    <CalendarMonthIcon fontSize="small" /> {formartDate(item.createdDate, 'short')}
                                  </Typography>
                                </div>
                              </CardContent>
                            </Card>
                          ))}
                    </div>

                  </Box>

                </Box>
              </Grid>

            </Grid>

          </Box>
        </Box>
      }
    </>
  );
}






// <Grid item xs={3}>
// <Box sx={{
//   width: '100%',
//   height: '500px',
// }}>
//   <Box sx={{
//     bgcolor: '#F6F5F2',
//     borderRadius: '6px',
//     height: 'fit-content',
//     maxHeight: '650px'
//   }}>
//     <Box sx={{
//       height: '50px',
//       p: 2,
//       display: 'flex',
//       alignItems: 'center',
//       justifyContent: 'space-between'
//     }}>
//       <Typography sx={{
//         fontWeight: 'bold',
//         cursor: 'pointer',
//         color: 'green'
//       }}>Tiếp Nhận ({ticketData.length})</Typography>

//       <FilterListIcon sx={{}} onClick={handleMenuOpen} />
//       <Menu
//         anchorEl={anchorEl}
//         open={Boolean(anchorEl)}
//         onClose={handleMenuClose}
//         transformOrigin={{ horizontal: 'right', vertical: 'top' }}
//         anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
//       >
//         <MenuItem >
//         </MenuItem>
//         <MenuItem >
//           C
//         </MenuItem>
//       </Menu>

//     </Box>
//     <Box sx={{
//       '&::-webkit-scrollbar:hover': {
//         display: 'display !important' // Override display property on hover
//       },
//       p: 2,
//       display: 'flex',
//       flexDirection: 'column',
//       gap: 1,
//       overflowX: 'hidden',
//       overflowY: 'auto', // Change 'overflow' to 'overflowY' for vertical scrolling
//       maxHeight: '520px'
//     }}>
//       <div>
//         {
//           ticketData && ticketData.filter((val) => {
//             if (selectedDate === null && search === null) {
//               return true;
//             }
//             if (selectedDate !== null && search !== null) {
//               console.log(format(val.createdBy, 'dd/MM/yyyy'));
//               console.log(format(selectedDate, 'dd/MM/yyyy'));
//               return format(val.createdBy, 'dd/MM/yyyy') === format(selectedDate, 'dd/MM/yyyy') && val.callId.includes(search);
//             }
//             if (selectedDate !== null && search === null) {
//               return format(val.createdBy, 'dd/MM/yyyy') === selectedDate.toLocaleDateString();
//             }
//             if (selectedDate === null && search !== null) {
//               return val.callId.includes(search);
//             }
//             return false;
//           })
//             .map(item => (
//               <Card
//                 key={item.id}
//                 sx={{
//                   cursor: 'pointer',
//                   boxShadow: '0 1px 1px rgba(0,0,0,0.2)',
//                   overflow: 'unset',
//                   marginBottom: '10px'
//                 }}
//               >
//                 <CardContent
//                   sx={{
//                     p: 1.5,
//                     '&:last-child': { p: 1.5 }
//                   }}
//                 >
//                   <p className='flex justify-between'>
//                     <Typography sx={{ fontSize: '12px', fontWeight: 'bold' }}>
//                       Sửa lại đường ống
//                     </Typography>
//                     <Typography sx={{ fontSize: '12px', fontWeight: 'bold', color: 'blue' }}>
//                       Detail
//                     </Typography>
//                   </p>
//                   <Typography sx={{ fontSize: '10px', fontWeight: 'bold' }}>
//                     <KeyIcon fontSize="small" /> TK32442{item.id}
//                   </Typography>
//                   <div className="flex justify-between">
//                     <Typography sx={{ fontSize: '10px', fontWeight: 'bold' }}>
//                       <PhoneIcon fontSize="small" /> {item.callId}
//                     </Typography>
//                     <Typography sx={{ fontSize: '10px', fontWeight: 'bold' }}>
//                       <CalendarMonthIcon fontSize="small" /> {formartDate(item.createdBy, 'short')}
//                     </Typography>
//                   </div>
//                 </CardContent>
//               </Card>
//             ))}
//       </div>

//     </Box>
//   </Box>

// </Box>
// </Grid>

// <Grid item xs={3}>
// <Box sx={{
//   bgcolor: '#F6F5F2',
//   ml: 2,
//   borderRadius: '6px',
//   height: 'fit-content',
//   maxHeight: '600px'
// }}>
//   <Box sx={{
//     height: '50px',
//     p: 2,
//     display: 'flex',
//     alignItems: 'center',
//     justifyContent: 'space-between'
//   }}>
//     <Typography sx={{
//       fontWeight: 'bold',
//       cursor: 'pointer',
//       color: '#F9C46B'
//     }}>Đang Xử Lý ({ticketData1.length})</Typography>
//     <FilterListIcon sx={{}} />
//   </Box>
//   <Box sx={{
//     '&::-webkit-scrollbar:hover': {
//       display: 'display !important' // Override display property on hover
//     },
//     p: 2,
//     display: 'flex',
//     flexDirection: 'column',
//     gap: 1,
//     overflowX: 'hidden',
//     overflowY: 'auto', // Change 'overflow' to 'overflowY' for vertical scrolling
//     maxHeight: '520px'
//   }}>
//     <div>
//       {
//        ticketData1 && ticketData1.filter((val) => {
//           if (selectedDate === null && search === null) {
//             return true;
//           }
//           if (selectedDate !== null && search !== null) {
//             console.log(format(val.createdBy, 'dd/MM/yyyy'));
//             console.log(format(selectedDate, 'dd/MM/yyyy'));
//             return format(val.createdBy, 'dd/MM/yyyy') === format(selectedDate, 'dd/MM/yyyy') && val.callId.includes(search);
//           }
//           if (selectedDate !== null && search === null) {
//             return format(val.createdBy, 'dd/MM/yyyy') === selectedDate.toLocaleDateString();
//           }
//           if (selectedDate === null && search !== null) {
//             return val.callId.includes(search);
//           }
//           return false;
//         })

//           .map(item => (
//             <Card
//               key={item.id}
//               sx={{
//                 cursor: 'pointer',
//                 boxShadow: '0 1px 1px rgba(0,0,0,0.2)',
//                 overflow: 'unset',
//                 marginBottom: '10px'
//               }}
//             >
//               <CardContent
//                 sx={{
//                   p: 1.5,
//                   '&:last-child': { p: 1.5 }
//                 }}
//               >
//                 <p className='flex justify-between'>
//                   <Typography sx={{ fontSize: '12px', fontWeight: 'bold' }}>
//                     Sửa lại đường ống
//                   </Typography>
//                   <Typography sx={{ fontSize: '12px', fontWeight: 'bold', color: 'blue' }}>
//                     Detail
//                   </Typography>
//                 </p>
//                 <Typography sx={{ fontSize: '10px', fontWeight: 'bold' }}>
//                   <KeyIcon fontSize="small" /> TK32132{item.id}
//                 </Typography>
//                 <div className="flex justify-between">
//                   <Typography sx={{ fontSize: '10px', fontWeight: 'bold' }}>
//                     <PhoneIcon fontSize="small" /> {item.callId}
//                   </Typography>
//                   <Typography sx={{ fontSize: '10px', fontWeight: 'bold' }}>
//                     <CalendarMonthIcon fontSize="small" />{formartDate(item.createdBy, 'short')}
//                   </Typography>
//                 </div>
//               </CardContent>
//             </Card>
//           ))}
//     </div>

//   </Box>

// </Box>
// </Grid>

// <Grid item xs={3}>
// <Box sx={{
//   bgcolor: '#F6F5F2',
//   ml: 2,
//   borderRadius: '6px',
//   height: 'fit-content',
//   maxHeight: '600px'
// }}>
//   <Box sx={{
//     height: '50px',
//     p: 2,
//     display: 'flex',
//     alignItems: 'center',
//     justifyContent: 'space-between'
//   }}>
//     <Typography sx={{
//       fontWeight: 'bold',
//       cursor: 'pointer',
//       color: '#2A6FDB'
//     }}>Đã Xử Lý ({ticketData2.length})</Typography>
//     <FilterListIcon sx={{}} />
//   </Box>
//   <Box sx={{
//     '&::-webkit-scrollbar:hover': {
//       display: 'display !important' // Override display property on hover
//     },
//     p: 2,
//     display: 'flex',
//     flexDirection: 'column',
//     gap: 1,
//     overflowX: 'hidden',
//     overflowY: 'auto', // Change 'overflow' to 'overflowY' for vertical scrolling
//     maxHeight: '520px'
//   }}>
//     <div>
//       {
//         ticketData2 && ticketData2.filter((val) => {
//           if (selectedDate === null && search === null) {
//             return true;
//           }
//           if (selectedDate !== null && search !== null) {
//             console.log(format(val.createdBy, 'dd/MM/yyyy'));
//             console.log(format(selectedDate, 'dd/MM/yyyy'));
//             return format(val.createdBy, 'dd/MM/yyyy') === format(selectedDate, 'dd/MM/yyyy') && val.callId.includes(search);
//           }
//           if (selectedDate !== null && search === null) {
//             return format(val.createdBy, 'dd/MM/yyyy') === selectedDate.toLocaleDateString();
//           }
//           if (selectedDate === null && search !== null) {
//             return val.callId.includes(search);
//           }
//           return false;
//         })

//           .map(item => (
//             <Card
//               key={item.id}
//               sx={{
//                 cursor: 'pointer',
//                 boxShadow: '0 1px 1px rgba(0,0,0,0.2)',
//                 overflow: 'unset',
//                 marginBottom: '10px'
//               }}
//             >
//               <CardContent
//                 sx={{
//                   p: 1.5,
//                   '&:last-child': { p: 1.5 }
//                 }}
//               >
//                 <p className='flex justify-between'>
//                   <Typography sx={{ fontSize: '12px', fontWeight: 'bold' }}>
//                     Sửa lại đường ống
//                   </Typography>
//                   <Typography sx={{ fontSize: '12px', fontWeight: 'bold', color: 'blue' }}>
//                     Detail
//                   </Typography>
//                 </p>
//                 <Typography sx={{ fontSize: '10px', fontWeight: 'bold' }}>
//                   <KeyIcon fontSize="small" /> TK32132{item.id}
//                 </Typography>
//                 <div className="flex justify-between">
//                   <Typography sx={{ fontSize: '10px', fontWeight: 'bold' }}>
//                     <PhoneIcon fontSize="small" /> {item.callId}
//                   </Typography>
//                   <Typography sx={{ fontSize: '10px', fontWeight: 'bold' }}>
//                     <CalendarMonthIcon fontSize="small" /> {formartDate(item.createdBy, 'short')}
//                   </Typography>
//                 </div>
//               </CardContent>
//             </Card>
//           ))}
//     </div>

//   </Box>

// </Box>
// </Grid>
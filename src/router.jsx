import { createBrowserRouter, Outlet, Navigate } from 'react-router-dom';
import { Toaster } from 'react-hot-toast';
import { lazy, Suspense } from 'react';
// // //Components
import LoginPage from './pages/Auth/LoginPage';
// //Import Layout
import { AuthLayout } from './layouts/AuthLayout/AuthLayout';
import { RootLayout } from './layouts/RootLayout/RootLayout';
import { ManagerLayout } from './layouts/ManagerLayout/ManagerLayout';
import { AdminLayout } from './layouts/AdminLayout/AdminLayout'; 

import { AuthProvider } from './context/AuthContext';
import AxiosProvider from './context/AxiosContex';
import { SipProvider } from './context/SipContext';
import LoadingLazy from './components/LoadingPage/LoadingLazy';


const StaffManager = lazy(() => import('./pages/Staff/PageStaff/StaffManager'));
const Ticket = lazy(() => import('./pages/Staff/Ticket'));
const Schedule = lazy(() => import('./pages/Staff/Schedule'));
const HistoryCall = lazy(() => import('./pages/Staff/CallHistory'));
const HistoryCall2 = lazy(() => import('./pages/Manager/CallHistory'));
const MissCall = lazy(() => import('./pages/Staff/MissCall'));
const ManagerManager = lazy(() => import('./pages/Manager/PageManager'));
const CustomerReport = lazy(() => import('./pages/Manager/CustomerReport'));
const ManagerTicet = lazy(() => import('./pages/Manager/TicketReport'));
const ManagerTicket = lazy(() => import('./pages/Manager/Ticket'));
const KpiTicket = lazy(() => import('./pages/Manager/KpiTicket'));
const AcceptCustomer = lazy(() => import('./pages/Manager/AcceptCustomer'));
const AdminManager = lazy(() => import('./pages/Admin/PageAdmin'));
const Customer = lazy(() => import('./pages/Admin/Customer'));
const Config = lazy(() => import('./pages/Admin/Config'));
const Extension = lazy(() => import('./pages/Admin/Extension'));
const Level = lazy(() => import('./pages/Admin/Level'));
const Source = lazy(() => import('./pages/Admin/Source'));
const CustomerLevel = lazy(() => import('./pages/Admin/CustomerLevel'));
const TicketTag = lazy(() => import('./pages/Admin/TagTicket'));
const TicketDetail = lazy(() => import('./pages/Detail/TicketDetail'));
const CreatedTicket = lazy(() => import('./pages/Detail/Ticket'));

// // import AdminManager from './pages/Admin/PageAdmin';
// // import Customer from './pages/Admin/Customer';
// // import Config from './pages/Admin/Config';
// // import Extension from './pages/Admin/Extension';
// // import Level from './pages/Admin/Level';
// // import Source from './pages/Admin/Source';


// // import { HistoryCall } from './pages/Staff/CallHistory';
// // import { Schedule } from './pages/Staff/Schedule';
// // import { Ticket } from './pages/Staff/Ticket';
// // import ManagerManager from './pages/Manager/PageManager';
// // import TicketPage from './pages/DetailsUser/Ticket';
// // //Page Admin




// // import { HistoryCall } from './pages/Staff/CallHistory';




export const router = createBrowserRouter([
  {
    element: <ContextWrapper />,
    children: [
      {
        path: '*',
        element: (
          <Navigate
            to='/login'
            replace
          />
        ),
      },
      {
        element: <AuthLayout />,
        children: [
          { path: 'login', element: <LoginPage /> },
        ],
      },
      {
        element: <RootLayout />,
        children: [
          { path: '/:id/:idUser/ticket',
          element: (
            <Suspense fallback={<LoadingLazy />}>
              <TicketDetail />
            </Suspense>
          ),
          },
          { path: '/:id/:idUser/:idCon/ticket',
          element: (
            <Suspense fallback={<LoadingLazy />}>
              <CreatedTicket />
            </Suspense>
          ),
          },
          { path: '/staff',
          element: (
            <Suspense fallback={<LoadingLazy />}>
              <StaffManager />
            </Suspense>
          ),
          },
          { path: '/staff/schudule',
          element: (
            <Suspense fallback={<LoadingLazy />}>
              <Schedule />
            </Suspense>
          ),
          },
          { path: '/staff/ticket',
          element: (
            <Suspense fallback={<LoadingLazy />}>
              <Ticket />
            </Suspense>
          ),
          },
          { path: '/staff/callhistory',
          element: (
            <Suspense fallback={<LoadingLazy />}>
              <HistoryCall />
            </Suspense>
          ),
          },
          { path: '/staff/misscall',
          element: (
            <Suspense fallback={<LoadingLazy />}>
              <MissCall />
            </Suspense>
          ),
          }
          // { path: '/staff/:id/:code/ticket', element: <TicketPage /> },
        ],
      },
      {
        element: <ManagerLayout />,
        children: [
          { path: '/:id/:idUser/ticket',
          element: (
            <Suspense fallback={<LoadingLazy />}>
              <TicketDetail />
            </Suspense>
          ),
          },
          { path: '/manager/tickets',
          element: (
            <Suspense fallback={<LoadingLazy />}>
              <ManagerTicket />
            </Suspense>
          ),
          },
          { path: '/manager',
          element: (
            <Suspense fallback={<LoadingLazy />}>
              <ManagerManager />
            </Suspense>
          ),
          },
          { path: '/manager/kpiticket',
          element: (
            <Suspense fallback={<LoadingLazy />}>
              <KpiTicket />
            </Suspense>
          ),
          },
          { path: '/manager/acceptedrequest',
          element: (
            <Suspense fallback={<LoadingLazy />}>
              <AcceptCustomer />
            </Suspense>
          ),
          },
          { path: '/manager/staff',
          element: (
            <Suspense fallback={<LoadingLazy />}>
              <CustomerReport />
            </Suspense>
          ),
          },
          { path: '/manager/ticket',
          element: (
            <Suspense fallback={<LoadingLazy />}>
              <ManagerTicet />
            </Suspense>
          ),
          },
          { path: '/manager/m',
          element: (
            <Suspense fallback={<LoadingLazy />}>
              <StaffManager />
            </Suspense>
          ),
          },
          { path: '/manager/mschudule',
          element: (
            <Suspense fallback={<LoadingLazy />}>
              <Schedule />
            </Suspense>
          ),
          },
          { path: '/manager/mticket',
          element: (
            <Suspense fallback={<LoadingLazy />}>
              <Ticket />
            </Suspense>
          ),
          },
          { path: '/manager/mcallhistory',
          element: (
            <Suspense fallback={<LoadingLazy />}>
              <HistoryCall2 />
            </Suspense>
          ),
          },
          { path: '/manager/mmisscall',
          element: (
            <Suspense fallback={<LoadingLazy />}>
              <MissCall />
            </Suspense>
          ),
          }


        ],
      },
      {
        element: <AdminLayout />,
        children: [
          { path: '/admin',
          element: (
            <Suspense fallback={<LoadingLazy />}>
              <AdminManager />
            </Suspense>
          ),
          },
          { path: '/admin/configuration',
          element: (
            <Suspense fallback={<LoadingLazy />}>
              <Config />
            </Suspense>
          ),
          },
          { path: '/admin/extension',
          element: (
            <Suspense fallback={<LoadingLazy />}>
              <Extension />
            </Suspense>
          ),
          },
          { path: '/admin/user',
          element: (
            <Suspense fallback={<LoadingLazy />}>
              <Customer />
            </Suspense>
          ),
          },
          { path: '/admin/level',
          element: (
            <Suspense fallback={<LoadingLazy />}>
              <Level />
            </Suspense>
          ),
          },
          { path: '/admin/source',
          element: (
            <Suspense fallback={<LoadingLazy />}>
              <Source />
            </Suspense>
          ),
          },
          { path: '/admin/customerlevel',
          element: (
            <Suspense fallback={<LoadingLazy />}>
              <CustomerLevel />
            </Suspense>
          ),
          },
          { path: '/admin/tickettag',
          element: (
            <Suspense fallback={<LoadingLazy />}>
              <TicketTag />
            </Suspense>
          ),
          },
          
        ],
      },
    ],
  },
]);

function ContextWrapper() {
  return (
    <AxiosProvider>
    <AuthProvider>
      <SipProvider>
        <Toaster
          position='bottom-right'
          reverseOrder={true}
        />
        <Outlet />
      </SipProvider>
    </AuthProvider>
  </AxiosProvider>
  );
}

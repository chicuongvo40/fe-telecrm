// Styles
import styles from './EditCampaignAdm.module.css';
// Icons
import { AiOutlineClose } from 'react-icons/ai';
// Components
import { Input } from '~/components/Material/Input';
import { Button } from '~/components/Material/Button';
import { useQuery, useMutation } from '@tanstack/react-query';
// Modules
import { motion } from 'framer-motion';
import { useAxios } from '~/context/AxiosContex';
//functions
import { useState } from 'react';
import { useEffect } from 'react';
import { toast } from 'react-hot-toast';
import { useRef } from 'react';
import { useAuth } from '~/context/AuthContext';
export default function EditCampaignAdm({ idBrand, setEditModal, refetch}) {
  const { updateTicketByManager,getTicketById,getUserByStatus,getTicketIdById } = useAxios();
  const { user } = useAuth();
  const ods = useRef();
  const [branchName, setBrandName] = useState('');
  const {
    data: source ,
  } = useQuery(
    {
      queryKey: ['source'],
      queryFn: async () => await getUserByStatus(Object.values(user)[4]),
    }
  );
  const {
    data: getBrandId,
  } = useQuery(
    {
      queryKey: ['getBrandId'],
      queryFn: async () => await getTicketIdById(idBrand),
    }
  );
 //* update brand mutations
 const updateBrandMutation = useMutation({
  mutationFn: (data) => {
    console.log(data);
    return updateTicketByManager(data).then((res) => {
      
      return res;
    });
  },
  onSuccess: (data) => { 
    console.log(data);
    if (data == '204') {
      toast('Update thành công', {
        icon: '👏',
        style: {
          borderRadius: '10px',
          background: '#333',
          color: '#fff',
        },
      });
      refetch();
      setEditModal(false);
    }
  },
});
function handleUpdateBrand(e) {
  e.preventDefault();
  console.log(getBrandId);
  const dataPatch = {
      id: getBrandId.id,
      branchId: getBrandId.branchId,
      note: getBrandId.note,
      customerId: getBrandId.customerId,
      ticketTypeId: getBrandId.ticketTypeId,
      levelId: getBrandId.levelId,
      ticketStatusId: 2,
      createdDate: getBrandId.createdDate,
      createdBy: getBrandId.createdBy,
      assignedUserId: branchName,
      code: getBrandId.code,
      title: getBrandId.title,
  };
  if (
  
    !branchName 
  ) {
    toast('Không được bỏ trống mục nào', {
      icon: '👏',
      style: {
        borderRadius: '10px',
        background: '#333',
        color: '#fff',
      },
    });
    return;
  }
  updateBrandMutation.mutate({ data: dataPatch });
}
  return (
    <motion.div
      initial={{ opacity: 0, transition: 0.5 }}
      animate={{ opacity: 1, transition: 0.5 }}
      transition={{ type: 'spring' }}
      className={styles.formmoal}
    >
      <form>
        <div className={styles.head}>
          <h6>Thêm Nhân Viên</h6>
          <AiOutlineClose
            onClick={() => setEditModal(false)}
            style={{ color: 'grey', fontSize: '20px', cursor: 'pointer' }}
          />
        </div>
        <div className={styles.fields}>
        <div className={styles.field}>
            <label>Nhân Viên</label>
            <select onChange={(e) => setBrandName(e.target.value)} >
              {source &&
                source.map((lv, index) => {
                  if (lv?.id) {
                    return (
                      <option
                        key={index}
                        value={lv.id}
                      >
                        {lv.lastName} {lv.firstName}
                      </option>
                    );
                  }
                })}
            </select>
          </div>

         
          <div className={styles.buttons}>
            <Button
              className={styles.cancelbtn}
              onClick={(e) => {
                e.preventDefault();
                setEditModal(false);
              }}
            >
              HỦY
            </Button>
            <Button
              onClick={handleUpdateBrand}
              className={styles.addbtn}
            >
              Thêm Nhân Viên
            </Button>
          </div>
        </div>
      </form>
    </motion.div>
  );
}
